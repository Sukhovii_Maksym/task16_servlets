package domain;

public enum Toppings {
    blue_cheese, parmigiana, mozzarella, onion, salami, ham, olives, pepper, tomato
}
