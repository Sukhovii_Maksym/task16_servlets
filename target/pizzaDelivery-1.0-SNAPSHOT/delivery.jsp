<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <title>Pizza DeliverY</title>
    <link rel="stylesheet"
    	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
   	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
   	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src ="delivery.js"></script>
</head>
<body>
    <h1>Hello!!!</h1>
    <h2>Choose pizza:</h2>
    <div class ="d-flex justify-content-between">
    <div class=>
     <c:forEach items ="${pizzas}" var ="pizza">
     <div class="card text-center border border-dark mb-3" style="width: 18rem;">
       <div class="card-body">
         <h5 class="card-title">${pizza.name}</h5>
         <p class="card-text">${pizza.toppings}</p>
         <p class="card-text cost">${pizza.cost}</p>
         <input type ="button" class="btn btn-primary buy" value ="Buy"></input>
       </div>
     </div>
    </c:forEach>
    </div>
    <div class="">
    <h2> Add toppings:</h2>
    <h3> Everything by 7.5</h3>
    <table>
    <c:forEach items ="${toppings}" var ="topping">
    <tr>
    <td class="topping">${topping}</td>
    <td><input type="button"  class="addTopping" value="+" > </td>
    </tr>
    </c:forEach>
    </table>
    </div>
    <div class="" >
    <h3>Your order:</h3>
    <table class="table table-hover table-dark">
    <th>Base</th>
    <th>You added:</th>
    <th>Total Price:</th>
    <tr>
    <td id="base"></td>
    <td id="toppings"></td>
    <td id="totalPrice"></td>
    <tr>
    </table>
    <form>
    <input id="address" class="form-control" type="text" placeholder="Input your address here">
    <input type="button" class="btn btn-dark orderButton" value ="Order"></input>
    </form>
</body>
</html>